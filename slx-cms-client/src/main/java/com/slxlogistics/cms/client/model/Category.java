package com.slxlogistics.cms.client.model;

import java.io.Serializable;
import java.util.Map;
public class Category implements Serializable {
    private String name = null;
    private String description = null;
    private Map<String, String> attributes = null;
    private Boolean deleted = null;
    private Long id = null;
    public Category() {
    }

    public Category(Builder builder) {
        this.name = builder.name;
        this.description = builder.description;
        this.attributes = builder.attributes;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Category {\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("  description: ").append(description).append("\n");
        sb.append("  attributes: ").append(attributes).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private String name = null;

        private String description = null;

        private Map<String, String> attributes = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setAttributes(Map<String, String> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Category build() {
            return new Category(this); 
        }
    }
}

