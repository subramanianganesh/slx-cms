package com.slxlogistics.cms.client.model;

import java.io.Serializable;
public class ServiceResult implements Serializable {
    private Long serviceId = null;
    private Double costPrice = null;
    private Double salePrice = null;
    private Long serviceabilityId = null;
    public ServiceResult() {
    }

    public ServiceResult(Builder builder) {
        this.serviceId = builder.serviceId;
        this.costPrice = builder.costPrice;
        this.salePrice = builder.salePrice;
        this.serviceabilityId = builder.serviceabilityId;
        }

    public Long getServiceId() {
        return serviceId;
    }
    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Double getCostPrice() {
        return costPrice;
    }
    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Double getSalePrice() {
        return salePrice;
    }
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Long getServiceabilityId() {
        return serviceabilityId;
    }
    public void setServiceabilityId(Long serviceabilityId) {
        this.serviceabilityId = serviceabilityId;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class ServiceResult {\n");
        sb.append("  serviceId: ").append(serviceId).append("\n");
        sb.append("  costPrice: ").append(costPrice).append("\n");
        sb.append("  salePrice: ").append(salePrice).append("\n");
        sb.append("  serviceabilityId: ").append(serviceabilityId).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Long serviceId = null;

        private Double costPrice = null;

        private Double salePrice = null;

        private Long serviceabilityId = null;

        public Builder setServiceId(Long serviceId) {
            this.serviceId = serviceId;
            return this;
        }

        public Builder setCostPrice(Double costPrice) {
            this.costPrice = costPrice;
            return this;
        }

        public Builder setSalePrice(Double salePrice) {
            this.salePrice = salePrice;
            return this;
        }

        public Builder setServiceabilityId(Long serviceabilityId) {
            this.serviceabilityId = serviceabilityId;
            return this;
        }

        public ServiceResult build() {
            return new ServiceResult(this); 
        }
    }
}

