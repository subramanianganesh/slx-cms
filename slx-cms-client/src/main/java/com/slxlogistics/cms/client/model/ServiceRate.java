package com.slxlogistics.cms.client.model;

import java.io.Serializable;
public class ServiceRate implements Serializable {
    private Double minimumWeight = null;
    private Double maximumWeight = null;
    private Double additionalWeight = null;
    private Double costPrice = null;
    private Double salePrice = null;
    private Boolean deleted = null;
    private Long id = null;
    public ServiceRate() {
    }

    public ServiceRate(Builder builder) {
        this.minimumWeight = builder.minimumWeight;
        this.maximumWeight = builder.maximumWeight;
        this.additionalWeight = builder.additionalWeight;
        this.costPrice = builder.costPrice;
        this.salePrice = builder.salePrice;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public Double getMinimumWeight() {
        return minimumWeight;
    }
    public void setMinimumWeight(Double minimumWeight) {
        this.minimumWeight = minimumWeight;
    }

    public Double getMaximumWeight() {
        return maximumWeight;
    }
    public void setMaximumWeight(Double maximumWeight) {
        this.maximumWeight = maximumWeight;
    }

    public Double getAdditionalWeight() {
        return additionalWeight;
    }
    public void setAdditionalWeight(Double additionalWeight) {
        this.additionalWeight = additionalWeight;
    }

    public Double getCostPrice() {
        return costPrice;
    }
    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Double getSalePrice() {
        return salePrice;
    }
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class ServiceRate {\n");
        sb.append("  minimumWeight: ").append(minimumWeight).append("\n");
        sb.append("  maximumWeight: ").append(maximumWeight).append("\n");
        sb.append("  additionalWeight: ").append(additionalWeight).append("\n");
        sb.append("  costPrice: ").append(costPrice).append("\n");
        sb.append("  salePrice: ").append(salePrice).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Double minimumWeight = null;

        private Double maximumWeight = null;

        private Double additionalWeight = null;

        private Double costPrice = null;

        private Double salePrice = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setMinimumWeight(Double minimumWeight) {
            this.minimumWeight = minimumWeight;
            return this;
        }

        public Builder setMaximumWeight(Double maximumWeight) {
            this.maximumWeight = maximumWeight;
            return this;
        }

        public Builder setAdditionalWeight(Double additionalWeight) {
            this.additionalWeight = additionalWeight;
            return this;
        }

        public Builder setCostPrice(Double costPrice) {
            this.costPrice = costPrice;
            return this;
        }

        public Builder setSalePrice(Double salePrice) {
            this.salePrice = salePrice;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public ServiceRate build() {
            return new ServiceRate(this); 
        }
    }
}

