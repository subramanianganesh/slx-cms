package com.slxlogistics.cms.client.model;

import java.io.Serializable;
import com.slxlogistics.cms.client.model.Service;
public class Serviceability implements Serializable {
    private Service service = null;
    private String sourceGeoId = null;
    private String destinationGeoId = null;
    private PackageType packageType = null;
    public enum PackageType { document, parcel, }; 
    private Boolean deleted = null;
    private Long id = null;
    public Serviceability() {
    }

    public Serviceability(Builder builder) {
        this.service = builder.service;
        this.sourceGeoId = builder.sourceGeoId;
        this.destinationGeoId = builder.destinationGeoId;
        this.packageType = builder.packageType;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public Service getService() {
        return service;
    }
    public void setService(Service service) {
        this.service = service;
    }

    public String getSourceGeoId() {
        return sourceGeoId;
    }
    public void setSourceGeoId(String sourceGeoId) {
        this.sourceGeoId = sourceGeoId;
    }

    public String getDestinationGeoId() {
        return destinationGeoId;
    }
    public void setDestinationGeoId(String destinationGeoId) {
        this.destinationGeoId = destinationGeoId;
    }

    public PackageType getPackageType() {
        return packageType;
    }
    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Serviceability {\n");
        sb.append("  service: ").append(service).append("\n");
        sb.append("  sourceGeoId: ").append(sourceGeoId).append("\n");
        sb.append("  destinationGeoId: ").append(destinationGeoId).append("\n");
        sb.append("  packageType: ").append(packageType).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private Service service = null;

        private String sourceGeoId = null;

        private String destinationGeoId = null;

        private PackageType packageType = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setService(Service service) {
            this.service = service;
            return this;
        }

        public Builder setSourceGeoId(String sourceGeoId) {
            this.sourceGeoId = sourceGeoId;
            return this;
        }

        public Builder setDestinationGeoId(String destinationGeoId) {
            this.destinationGeoId = destinationGeoId;
            return this;
        }

        public Builder setPackageType(PackageType packageType) {
            this.packageType = packageType;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Serviceability build() {
            return new Serviceability(this); 
        }
    }
}

