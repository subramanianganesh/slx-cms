package com.slxlogistics.cms.client.model;

import java.io.Serializable;
import java.util.Map;
public class Service implements Serializable {
    private String name = null;
    private Long providerId = null;
    private String description = null;
    private Long categoryId = null;
    private String metaDescription = null;
    private String metaKeywords = null;
    private Boolean enabled = null;
    private Map<String, String> attributes = null;
    private Boolean deleted = null;
    private Long id = null;
    public Service() {
    }

    public Service(Builder builder) {
        this.name = builder.name;
        this.providerId = builder.providerId;
        this.description = builder.description;
        this.categoryId = builder.categoryId;
        this.metaDescription = builder.metaDescription;
        this.metaKeywords = builder.metaKeywords;
        this.enabled = builder.enabled;
        this.attributes = builder.attributes;
        this.deleted = builder.deleted;
        this.id = builder.id;
        }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Long getProviderId() {
        return providerId;
    }
    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getMetaDescription() {
        return metaDescription;
    }
    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }
    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Service {\n");
        sb.append("  name: ").append(name).append("\n");
        sb.append("  providerId: ").append(providerId).append("\n");
        sb.append("  description: ").append(description).append("\n");
        sb.append("  categoryId: ").append(categoryId).append("\n");
        sb.append("  metaDescription: ").append(metaDescription).append("\n");
        sb.append("  metaKeywords: ").append(metaKeywords).append("\n");
        sb.append("  enabled: ").append(enabled).append("\n");
        sb.append("  attributes: ").append(attributes).append("\n");
        sb.append("  deleted: ").append(deleted).append("\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    public static class Builder {
        private String name = null;

        private Long providerId = null;

        private String description = null;

        private Long categoryId = null;

        private String metaDescription = null;

        private String metaKeywords = null;

        private Boolean enabled = null;

        private Map<String, String> attributes = null;

        private Boolean deleted = null;

        private Long id = null;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setProviderId(Long providerId) {
            this.providerId = providerId;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setCategoryId(Long categoryId) {
            this.categoryId = categoryId;
            return this;
        }

        public Builder setMetaDescription(String metaDescription) {
            this.metaDescription = metaDescription;
            return this;
        }

        public Builder setMetaKeywords(String metaKeywords) {
            this.metaKeywords = metaKeywords;
            return this;
        }

        public Builder setEnabled(Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder setAttributes(Map<String, String> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder setDeleted(Boolean deleted) {
            this.deleted = deleted;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Service build() {
            return new Service(this); 
        }
    }
}

