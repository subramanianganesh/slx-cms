package com.slxlogistics.cms.client.api;

import com.slxlogistics.commons.client.ApiException;
import com.slxlogistics.commons.client.ApiInvoker;

import com.slxlogistics.cms.client.model.Service;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import java.util.*;

/**
 * Auto Generated by Swagger Codegen
 */
public class ServicesApi {
    
    private String basePath;
    
    private ApiInvoker apiInvoker = ApiInvoker.getInstance();

    /**
     * Default constructor
     * 
     * @param basePath
     */
    public ServicesApi(String basePath) {
        this.basePath = basePath;
    }

    /**
     * Returns the api invoker
     * 
     * @return apiInvoker
     */
    public ApiInvoker getInvoker() {
        return apiInvoker;
    }

    /**
     * @param basePath
     */
    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    /**
     * @return basePath
     */
    public String getBasePath() {
        return basePath;
    }

    //error info- code: 200 reason: "OK" model: Service
    public List<Service> listServices (ListServicesParams params) throws ApiException {
        Object postBody = null;
        // create path and map variables
        String path = "/services".replaceAll("\\{format\\}","json");

        // query params
        Map<String, String> queryParams = new HashMap<String, String>();
        Map<String, String> headerParams = new HashMap<String, String>();

        if(!"null".equals(String.valueOf(params.categoryId)))
            queryParams.put("category_id", String.valueOf(params.categoryId));
        if(!"null".equals(String.valueOf(params.providerId)))
            queryParams.put("provider_id", String.valueOf(params.providerId));
        if(!"null".equals(String.valueOf(params.exclude)))
            queryParams.put("exclude", String.valueOf(params.exclude));
        if(!"null".equals(String.valueOf(params.include)))
            queryParams.put("include", String.valueOf(params.include));
        if(!"null".equals(String.valueOf(params.perPage)))
            queryParams.put("per_page", String.valueOf(params.perPage));
        if(!"null".equals(String.valueOf(params.page)))
            queryParams.put("page", String.valueOf(params.page));
        String[] contentTypes = {
                "application/json"};

        String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";
        try {
            Response response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, postBody, headerParams, contentType);
            if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
                return null;
            }
            return response.readEntity(new GenericType<List<Service>>(){});
        } catch (ApiException ex) {
            if(ex.getCode() == Response.Status.NOT_FOUND.getStatusCode()) {
                return null;
            } else {
                throw ex;
            }
        }
    }

    /**
     * Parameters holder for the operation  listServices
     * 
     * @author Auto generated by swagger codegen
     */
    public static class ListServicesParams {
        private Integer categoryId;
        private Integer providerId;
        private String exclude;
        private String include;
        private Integer perPage;
        private Integer page;
        public ListServicesParams setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
            return this;
        }
        public ListServicesParams setProviderId(Integer providerId) {
            this.providerId = providerId;
            return this;
        }
        public ListServicesParams setExclude(String exclude) {
            this.exclude = exclude;
            return this;
        }
        public ListServicesParams setInclude(String include) {
            this.include = include;
            return this;
        }
        public ListServicesParams setPerPage(Integer perPage) {
            this.perPage = perPage;
            return this;
        }
        public ListServicesParams setPage(Integer page) {
            this.page = page;
            return this;
        }
        }
    //error info- code: 200 reason: "OK" model: Service
    //error info- code: 404 reason: "Not Found" model: <none>
    public Service readService (ReadServiceParams params) throws ApiException {
        Object postBody = null;
        // verify required params are set
        if(params.id == null ) {
            throw new ApiException(Response.Status.BAD_REQUEST.getStatusCode(), "missing required params");
        }
        // create path and map variables
        String path = "/services/{id}".replaceAll("\\{format\\}","json").replaceAll("\\{" + "id" + "\\}", apiInvoker.escapeString(params.id.toString()));

        // query params
        Map<String, String> queryParams = new HashMap<String, String>();
        Map<String, String> headerParams = new HashMap<String, String>();

        if(!"null".equals(String.valueOf(params.exclude)))
            queryParams.put("exclude", String.valueOf(params.exclude));
        if(!"null".equals(String.valueOf(params.include)))
            queryParams.put("include", String.valueOf(params.include));
        String[] contentTypes = {
                "application/json"};

        String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";
        try {
            Response response = apiInvoker.invokeAPI(basePath, path, "GET", queryParams, postBody, headerParams, contentType);
            if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
                return null;
            }
            return response.readEntity(new GenericType<Service>(){});
        } catch (ApiException ex) {
            if(ex.getCode() == Response.Status.NOT_FOUND.getStatusCode()) {
                return null;
            } else {
                throw ex;
            }
        }
    }

    /**
     * Parameters holder for the operation  readService
     * 
     * @author Auto generated by swagger codegen
     */
    public static class ReadServiceParams {
        private String id;
        private String exclude;
        private String include;
        public ReadServiceParams setId(String id) {
            this.id = id;
            return this;
        }
        public ReadServiceParams setExclude(String exclude) {
            this.exclude = exclude;
            return this;
        }
        public ReadServiceParams setInclude(String include) {
            this.include = include;
            return this;
        }
        }
    }

