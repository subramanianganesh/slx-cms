/**
 * 
 */
package com.slxlogistics.cms.domain;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * @author ganeshs
 *
 */
public class ServiceRateTest {

    @Test
    public void shouldComputePriceForFixedSlab() {
        ServiceRate rate = new ServiceRate();
        rate.setMinimumWeight(0d);
        rate.setMaximumWeight(100d);
        rate.setAdditionalWeight(0d);
        rate.setCostPrice(50d);
        rate.setSalePrice(10d);
        assertEquals(rate.computeCostPrice(99d), 50d);
        assertEquals(rate.computeSalePrice(99d), 10d);
    }
    
    @Test
    public void shouldNotComputePriceForFixedSlabWhenUnderWeight() {
        ServiceRate rate = new ServiceRate();
        rate.setMinimumWeight(10d);
        rate.setMaximumWeight(100d);
        rate.setAdditionalWeight(0d);
        rate.setCostPrice(50d);
        rate.setSalePrice(10d);
        assertEquals(rate.computeCostPrice(1d), 0d);
        assertEquals(rate.computeSalePrice(1d), 0d);
    }
    
    @Test
    public void shouldComputePriceWithAdditionalWeight() {
        ServiceRate rate = new ServiceRate();
        rate.setMinimumWeight(10d);
        rate.setMaximumWeight(100d);
        rate.setAdditionalWeight(3d);
        rate.setCostPrice(50d);
        rate.setSalePrice(10d);
        assertEquals(rate.computeCostPrice(40d), 500d);
        assertEquals(rate.computeSalePrice(40d), 100d);
    }
    
    @Test
    public void shouldNotComputePriceWithAdditionalWeightAndCeilPrice() {
        ServiceRate rate = new ServiceRate();
        rate.setMinimumWeight(10d);
        rate.setMaximumWeight(100d);
        rate.setAdditionalWeight(3d);
        rate.setCostPrice(5d);
        rate.setSalePrice(1d);
        assertEquals(rate.computeCostPrice(15d), 10d);
        assertEquals(rate.computeSalePrice(15d), 2d);
    }

}
