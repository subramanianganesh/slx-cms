/**
 * 
 */
package com.slxlogistics.cms.domain;

import static org.testng.Assert.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.google.common.collect.Sets;

/**
 * @author ganeshs
 *
 */
public class ServiceabilityTest {
    
    private Serviceability serviceability;
    
    private ServiceRate rate1;
    
    private ServiceRate rate2;
    
    @BeforeMethod
    public void setup() {
        serviceability = new Serviceability();
        rate1 = new ServiceRate();
        rate1.setMinimumWeight(0d);
        rate1.setMaximumWeight(10d);
        rate1.setAdditionalWeight(0d);
        rate1.setCostPrice(50d);
        rate1.setSalePrice(10d);
        rate2 = new ServiceRate();
        rate2.setMinimumWeight(10d);
        rate2.setMaximumWeight(100d);
        rate2.setAdditionalWeight(10d);
        rate2.setCostPrice(50d);
        rate2.setSalePrice(10d);
        serviceability.setRates(Sets.newHashSet(rate1, rate2));
    }

    @Test
    public void shouldComputeCostPrice() {
        assertEquals(serviceability.computeCostPrice(5d), 50d);
        assertEquals(serviceability.computeCostPrice(10d), 50d);
        assertEquals(serviceability.computeCostPrice(20d), 100d);
        assertEquals(serviceability.computeCostPrice(25d), 150d);
        assertEquals(serviceability.computeCostPrice(100d), 500d);
        assertEquals(serviceability.computeCostPrice(101d), 0d);
    }
    
    @Test
    public void shouldComputeSalePrice() {
        assertEquals(serviceability.computeSalePrice(5d), 10d);
        assertEquals(serviceability.computeSalePrice(10d), 10d);
        assertEquals(serviceability.computeSalePrice(20d), 20d);
        assertEquals(serviceability.computeSalePrice(25d), 30d);
        assertEquals(serviceability.computeSalePrice(100d), 100d);
        assertEquals(serviceability.computeSalePrice(101d), 0d);
    }
}
