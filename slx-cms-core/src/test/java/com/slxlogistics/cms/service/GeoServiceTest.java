/**
 * 
 */
package com.slxlogistics.cms.service;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.slxlogistics.geo.client.api.GeosApi;
import com.slxlogistics.geo.client.api.GeosApi.GetAncestorsParams;
import com.slxlogistics.geo.client.model.Geo;

/**
 * @author ganeshs
 *
 */
public class GeoServiceTest {
    
    private GeoService geoService;
    
    private GeosApi geosApi;
    
    private static WireMockServer wireMockServer;
    
    @BeforeClass
    public void beforeClass() {
        wireMockServer = new WireMockServer(wireMockConfig().bindAddress("localhost").port(8110)); //No-args constructor will start on port 8080, no HTTPS
        configureFor("localhost", 8110);
        wireMockServer.start();
    }
    
    @BeforeMethod
    public void setup() {
        geosApi = new GeosApi("http://localhost:8110");
        geoService = new GeoService(geosApi);
    }
    
    @AfterClass
    public void afterClass() {
        wireMockServer.stop();
    }

    @Test
    public void shouldGetCityForGeoId() {
        stubFor(get(urlEqualTo("/geos/1/ancestors?type=city")).willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json").withBody("[{\"id\":2}]")));
        GetAncestorsParams params = new GetAncestorsParams();
        params.setId(1L);
        params.setType("city");
        Geo geo = geoService.getCity(1L);
        assertEquals(geo.getId(), Long.valueOf(2));
    }
    
    @Test
    public void shouldNotGetCityForGeoIdWithoutCity() {
        stubFor(get(urlEqualTo("/geos/1/ancestors?type=city")).willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json").withBody("[]")));
        GetAncestorsParams params = new GetAncestorsParams();
        params.setId(1L);
        params.setType("city");
        Geo geo = geoService.getCity(1L);
        assertNull(geo);
    }
    
    @Test
    public void shouldGetServiceZonesForGeoId() {
        stubFor(get(urlEqualTo("/geos/1/ancestors?type=service_zone")).willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json").withBody("[{\"id\":2}]")));
        GetAncestorsParams params = new GetAncestorsParams();
        params.setId(1L);
        params.setType("service_zone");
        List<Geo> geos = geoService.getServiceZones(1L);
        assertFalse(geos.isEmpty());
        assertEquals(geos.get(0).getId(), Long.valueOf(2));
    }
}
