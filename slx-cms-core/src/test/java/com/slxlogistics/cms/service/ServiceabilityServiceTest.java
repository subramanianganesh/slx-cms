/**
 * 
 */
package com.slxlogistics.cms.service;

import org.activejpa.entity.testng.BaseModelTest;
import org.activejpa.jpa.JPA;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

/**
 * @author ganeshs
 *
 */
public class ServiceabilityServiceTest extends BaseModelTest {
    
    @BeforeClass
    public void beforeClass() {
        JPA.instance.addPersistenceUnit("test");
    }
    
    @BeforeMethod
    public void setup() throws Exception {
        super.setup();
    }

}
