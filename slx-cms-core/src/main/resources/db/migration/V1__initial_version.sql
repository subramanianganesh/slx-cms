CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `category_attributes` (
  category_id bigint(20) NOT NULL,
  value varchar(255) DEFAULT NULL,
  `key` varchar(255) NOT NULL,
  updated_at timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (category_id,`key`)
);

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `category_id` bigint(20) NOT NULL,
  `meta_description` varchar(2000) DEFAULT NULL,
  `meta_keywords` varchar(2000) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `service_attributes` (
  service_id bigint(20) NOT NULL,
  value varchar(255) DEFAULT NULL,
  `key` varchar(255) NOT NULL,
  updated_at timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (service_id,`key`)
);

CREATE TABLE `serviceabilities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20) NOT NULL,
  `source_geo_id` bigint(20) NOT NULL,
  `destination_geo_id` bigint(20) NOT NULL,
  `package_type` varchar(20) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `service_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `serviceability_id` bigint(20) NOT NULL,
  `minimum_weight` double NOT NULL,
  `maximum_weight` double NOT NULL,
  `additional_weight` double NOT NULL,
  `cost_price` double NOT NULL,
  `sale_price` double NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);