insert into categories (id, name, description, created_at) values (1, 'Same Day Delivery', null, now());
insert into categories (id, name, description, created_at) values (2, 'Next Day Delivery', null, now());

insert into services (id, provider_id, name, description, category_id, created_at) values (1, 1, 'Domestic Priority', null, 2, now());
insert into services (id, provider_id, name, description, category_id, created_at) values (2, 1, 'Dart Apex', null, 2, now());
insert into services (id, provider_id, name, description, category_id, created_at) values (3, 1, 'Dart Surface', null, 2, now());

insert into serviceabilities (id, service_id, source_geo_id, destination_geo_id, package_type, created_at) values (1, 1, 174863, 174861, 'document', now());
insert into serviceabilities (id, service_id, source_geo_id, destination_geo_id, package_type, created_at) values (2, 1, 174863, 174861, 'parcel', now());
insert into serviceabilities (id, service_id, source_geo_id, destination_geo_id, package_type, created_at) values (3, 1, 174863, 174862, 'document', now());
insert into serviceabilities (id, service_id, source_geo_id, destination_geo_id, package_type, created_at) values (4, 1, 174863, 174862, 'parcel', now());

insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (1, 0, 500, 0, 52.66, 52.66, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (1, 500, 10000, 500, 52.66, 52.66, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (2, 0, 500, 0, 52.66, 52.66, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (2, 500, 10000, 500, 52.66, 52.66, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (3, 0, 500, 0, 60.18, 60.18, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (3, 500, 10000, 500, 60.18, 60.18, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (4, 0, 500, 0, 60.18, 60.18, now());
insert into service_rates (serviceability_id, minimum_weight, maximum_weight, additional_weight, cost_price, sale_price, created_at) values (4, 500, 10000, 500, 60.18, 60.18, now());