/**
 * 
 */
package com.slxlogistics.cms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.slxlogistics.core.BaseAppConfig;
import com.slxlogistics.geo.client.api.GeosApi;

/**
 * @author ganeshs
 *
 */
@Configuration
@ComponentScan(basePackages={"com.slxlogistics"})
public class AppConfig extends BaseAppConfig {
    
    /**
     * @param location
     */
    public AppConfig() {
        super(new ClassPathResource("/META-INF/cms.yml"));
    }

    @Bean
    public GeosApi getGeosApi() {
        return new GeosApi(environment.getProperty("geoServiceBaseUrl"));
    }
}
