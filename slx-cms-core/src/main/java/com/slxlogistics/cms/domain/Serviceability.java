/**
 * 
 */
package com.slxlogistics.cms.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.minnal.instrument.entity.AggregateRoot;
import org.minnal.instrument.entity.Collection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Ordering;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@AggregateRoot(update=false, delete=false, create=false)
@Entity
@Table(name="serviceabilities")
@Access(AccessType.FIELD)
public class Serviceability extends BaseRecordableDomain {

    /**
     * @author ganeshs
     *
     */
    public enum PackageType {
        document, parcel
    }
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="serviceId")
    private Service service;

    private String sourceGeoId;
    
    private String destinationGeoId;
    
    @Enumerated(EnumType.STRING)
    private PackageType packageType = PackageType.document;
    
    @Collection(update=false, delete=false, create=false)
    @OneToMany(mappedBy="serviceability", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    @JsonIgnore
    private Set<ServiceRate> rates = new HashSet<ServiceRate>();

    /**
     * @return the service
     */
    public Service getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(Service service) {
        this.service = service;
    }

    /**
     * @return the sourceGeoId
     */
    public String getSourceGeoId() {
        return sourceGeoId;
    }

    /**
     * @param sourceGeoId the sourceGeoId to set
     */
    public void setSourceGeoId(String sourceGeoId) {
        this.sourceGeoId = sourceGeoId;
    }

    /**
     * @return the destinationGeoId
     */
    public String getDestinationGeoId() {
        return destinationGeoId;
    }

    /**
     * @param destinationGeoId the destinationGeoId to set
     */
    public void setDestinationGeoId(String destinationGeoId) {
        this.destinationGeoId = destinationGeoId;
    }

    /**
     * @return the packageType
     */
    public PackageType getPackageType() {
        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    /**
     * @return the rates
     */
    public Set<ServiceRate> getRates() {
        return rates;
    }

    /**
     * @param rates the rates to set
     */
    public void setRates(Set<ServiceRate> rates) {
        this.rates = rates;
    }
    
    /**
     * Adds the rate to the set
     * 
     * @param rate
     */
    public void addRate(ServiceRate rate) {
        rate.setServiceability(this);
        this.rates.add(rate);
    }
    
    /**
     * Computes the cost/sale price for the given weight
     * 
     * @param weight
     * @param costPrice cost or sale price
     * @return
     */
    public Double computePrice(Double weight, boolean costPrice) {
        Double price = 0d;
        Ordering<ServiceRate> ordering = new Ordering<ServiceRate>() {
            @Override
            public int compare(ServiceRate rate1, ServiceRate rate2) {
                return rate1.getMinimumWeight().compareTo(rate2.getMinimumWeight());
            }
        };
        Double minimumWeight = ordering.min(getRates()).getMinimumWeight();
        Double maximumWeight = ordering.max(getRates()).getMaximumWeight();
        if (weight < minimumWeight || weight > maximumWeight) {
            return 0d;
        }
        for (ServiceRate rate : ordering.sortedCopy(getRates())) {
            price += costPrice ? rate.computeCostPrice(weight) : rate.computeSalePrice(weight);
        }
        return price;
    }
    
    /**
     * Computes the cost price for the given weight
     * 
     * @param weight
     * @return
     */
    public Double computeCostPrice(Double weight) {
        return computePrice(weight, true);
    }
    
    /**
     * Computes the sale price for the given weight
     * 
     * @param weight
     * @return
     */
    public Double computeSalePrice(Double weight) {
        return computePrice(weight, false);
    }
}
