/**
 * 
 */
package com.slxlogistics.cms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.slxlogistics.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="service_rates")
@Access(AccessType.FIELD)
public class ServiceRate extends BaseRecordableDomain {
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="serviceabilityId")
    @JsonIgnore
    private Serviceability serviceability;

    @NotNull
    private Double minimumWeight;
    
    @NotNull
    private Double maximumWeight;
    
    @NotNull
    private Double additionalWeight;
    
    @NotNull
    private Double costPrice;
    
    @NotNull
    private Double salePrice;
    
    /**
     * @return the minimumWeight
     */
    public Double getMinimumWeight() {
        return minimumWeight;
    }

    /**
     * @param minimumWeight the minimumWeight to set
     */
    public void setMinimumWeight(Double minimumWeight) {
        this.minimumWeight = minimumWeight;
    }

    /**
     * @return the maximumWeight
     */
    public Double getMaximumWeight() {
        return maximumWeight;
    }

    /**
     * @param maximumWeight the maximumWeight to set
     */
    public void setMaximumWeight(Double maximumWeight) {
        this.maximumWeight = maximumWeight;
    }

    /**
     * @return the additionalWeight
     */
    public Double getAdditionalWeight() {
        return additionalWeight;
    }

    /**
     * @param additionalWeight the additionalWeight to set
     */
    public void setAdditionalWeight(Double additionalWeight) {
        this.additionalWeight = additionalWeight;
    }

    /**
     * @return the costPrice
     */
    public Double getCostPrice() {
        return costPrice;
    }

    /**
     * @param costPrice the costPrice to set
     */
    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return the salePrice
     */
    public Double getSalePrice() {
        return salePrice;
    }

    /**
     * @param salePrice the salePrice to set
     */
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * @return the serviceability
     */
    public Serviceability getServiceability() {
        return serviceability;
    }

    /**
     * @param serviceability the serviceability to set
     */
    public void setServiceability(Serviceability serviceability) {
        this.serviceability = serviceability;
    }

    /**
     * Computes the cost price for the given weight
     * 
     * @param weight
     * @return
     */
    private Double computePrice(Double weight, Double unitPrice) {
        if (weight < minimumWeight) {
            return 0d;
        }
        if (additionalWeight > 0) {
            weight -= minimumWeight;
            if (weight > maximumWeight) {
                weight = maximumWeight - weight;
            }
            return Math.ceil(weight / additionalWeight) * unitPrice;
        }
        return unitPrice;
    }

    /**
     * Computes the cost price for the given weight
     * 
     * @param weight
     * @return
     */
    public Double computeCostPrice(Double weight) {
        return computePrice(weight, costPrice);
    }
    
    /**
     * Computes the sale price for the given weight
     * 
     * @param weight
     * @return
     */
    public Double computeSalePrice(Double weight) {
        return computePrice(weight, salePrice);
    }
    
}
