/**
 * 
 */
package com.slxlogistics.cms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slxlogistics.geo.client.api.GeosApi;
import com.slxlogistics.geo.client.api.GeosApi.GetAncestorsParams;
import com.slxlogistics.geo.client.model.Geo;

/**
 * @author ganeshs
 *
 */
@Service
public class GeoService {
    
    @Autowired
    private GeosApi geosApi;
    
    /**
     * Default constructor
     */
    public GeoService() {
    }

    /**
     * @param geosApi
     */
    public GeoService(GeosApi geosApi) {
        this.geosApi = geosApi;
    }

    /**
     * Returns the city for the given geo id
     * 
     * @param geoId
     * @return
     */
    public Geo getCity(Long geoId) {
        GetAncestorsParams params = new GetAncestorsParams();
        params.setId(geoId);
        params.setType("city");
        List<Geo> geos = geosApi.getAncestors(params);
        if (geos == null || geos.isEmpty()) {
            return null;
        }
        return geos.get(0);
    }
    
    /**
     * Returns the service zones for the given geo id
     * 
     * @param geoId
     * @return
     */
    public List<Geo> getServiceZones(Long geoId) {
        GetAncestorsParams params = new GetAncestorsParams();
        params.setId(geoId);
        params.setType("service_zone");
        return geosApi.getAncestors(params);
    }
    
}
