/**
 * 
 */
package com.slxlogistics.cms.service;

import java.util.List;

import org.activejpa.entity.Condition.Operator;
import org.activejpa.entity.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.slxlogistics.cms.domain.Serviceability;
import com.slxlogistics.cms.domain.Serviceability.PackageType;
import com.slxlogistics.geo.client.model.Geo;

/**
 * @author ganeshs
 *
 */
@Service
public class ServiceabilityService {
    
    @Autowired
    private GeoService geoService;

    /**
     * Default constructor
     */
    public ServiceabilityService() {
    }

    /**
     * @param geoService
     */
    public ServiceabilityService(GeoService geoService) {
        this.geoService = geoService;
    }

    /**
     * @param fromGeoId
     * @param toGeoId
     * @param packageType
     * @param weight
     */
    public List<ServiceResult> searchServices(Long fromGeoId, Long toGeoId, PackageType packageType, final Double weight) {
        List<ServiceResult> services = Lists.newArrayList();
        Geo fromCity = geoService.getCity(fromGeoId);
        if (fromCity == null) {
            return services;
        }
        List<Geo> serviceZones = geoService.getServiceZones(toGeoId);
        if (serviceZones == null || serviceZones.isEmpty()) {
            return services;
        }
        Filter filter = new Filter();
        filter.addCondition("sourceGeoId", fromCity.getId());
        filter.addCondition("destinationGeoId", Operator.in, Lists.transform(serviceZones, new Function<Geo, Long>() {
            @Override
            public Long apply(Geo zone) {
                return zone.getId();
            }
        }));
        filter.addCondition("packageType", packageType);
        List<Serviceability> serviceabilities = Serviceability.where(filter);
        if (serviceabilities != null) {
            services.addAll(Lists.transform(serviceabilities, new Function<Serviceability, ServiceResult>() {
                @Override
                public ServiceResult apply(Serviceability serviceability) {
                    return new ServiceResult(serviceability, weight);
                }
            }));
        }
        return services;
    }
}
