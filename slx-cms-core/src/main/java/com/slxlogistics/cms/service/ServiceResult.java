/**
 * 
 */
package com.slxlogistics.cms.service;

import com.slxlogistics.cms.domain.Serviceability;

/**
 * @author ganeshs
 *
 */
public class ServiceResult {

    private Long serviceId;
    
    private Double costPrice;
    
    private Double salePrice;
    
    private Long serviceabilityId;

    /**
     * Default constructor
     */
    public ServiceResult() {
    }

    /**
     * @param service
     * @param costPrice
     * @param salePrice
     */
    public ServiceResult(Serviceability serviceability, Double weight) {
        this.serviceId = serviceability.getService().getId();
        this.serviceabilityId = serviceability.getId();
        this.costPrice = serviceability.computeCostPrice(weight);
        this.salePrice = serviceability.computeSalePrice(weight);
    }

    /**
     * @return the serviceId
     */
    public Long getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return the costPrice
     */
    public Double getCostPrice() {
        return costPrice;
    }

    /**
     * @param costPrice the costPrice to set
     */
    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    /**
     * @return the salePrice
     */
    public Double getSalePrice() {
        return salePrice;
    }

    /**
     * @param salePrice the salePrice to set
     */
    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * @return the serviceabilityId
     */
    public Long getServiceabilityId() {
        return serviceabilityId;
    }

    /**
     * @param serviceabilityId the serviceabilityId to set
     */
    public void setServiceabilityId(Long serviceabilityId) {
        this.serviceabilityId = serviceabilityId;
    }
}
