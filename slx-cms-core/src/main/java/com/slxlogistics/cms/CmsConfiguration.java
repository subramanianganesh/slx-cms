/**
 * 
 */
package com.slxlogistics.cms;

import org.minnal.core.config.ApplicationConfiguration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author ganeshs
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class CmsConfiguration extends ApplicationConfiguration {

    /**
     * Default constructor
     */
    public CmsConfiguration() {
    }

    /**
     * @param name
     */
    public CmsConfiguration(String name) {
        super(name);
    }

}
