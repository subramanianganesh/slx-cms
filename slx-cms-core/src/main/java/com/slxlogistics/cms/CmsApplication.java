/**
 * 
 */
package com.slxlogistics.cms;

import com.slxlogistics.core.SlxApplication;
import com.slxlogistics.spring.util.SpringUtil;

/**
 * @author ganeshs
 *
 */
public class CmsApplication extends SlxApplication<CmsConfiguration> {
    
    @Override
    public void init() {
        SpringUtil.load(AppConfig.class);
        super.init();
    }

}
