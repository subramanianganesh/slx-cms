/**
 * 
 */
package com.slxlogistics.cms.resource;

import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.common.base.Strings;
import com.slxlogistics.cms.domain.Serviceability;
import com.slxlogistics.cms.domain.Serviceability.PackageType;
import com.slxlogistics.cms.service.ServiceResult;
import com.slxlogistics.cms.service.ServiceabilityService;
import com.slxlogistics.spring.util.SpringUtil;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * @author ganeshs
 *
 */
@Path("/serviceabilities")
@Produces( { "application/json" })
public class ServiceabilityResource {

    private ServiceabilityService serviceabilityService;

    /**
     * Default constructor
     */
    public ServiceabilityResource() {
        this(SpringUtil.getBean(ServiceabilityService.class));
    }

    /**
     * @param serviceabilityService
     */
    public ServiceabilityResource(ServiceabilityService serviceabilityService) {
        this.serviceabilityService = serviceabilityService;
    }
    
    @Path("/promise")
    @ApiOperation(value="Returns the promise for the given pincodes and weight", response=ServiceResult.class, responseContainer="List")
    @GET
    public List<ServiceResult> getPromise(@QueryParam("from_geo_id") Long fromGeoId, @QueryParam("to_geo_id") Long toGeoId, @QueryParam("package_type") String packageType, 
            @QueryParam("weight") Double weight) {
        if (fromGeoId == null || toGeoId == null || weight == null || Strings.isNullOrEmpty(packageType)) {
            throw new BadRequestException("Missing from_geo_id/to_geo_id/package_type/weight");
        }
        return serviceabilityService.searchServices(fromGeoId, toGeoId, PackageType.valueOf(packageType), weight);
    }
    
    @Path("/{id}/promise")
    @ApiOperation(value="Search the available services for the given pincodes and weight", response=ServiceResult.class)
    @GET
    public ServiceResult getPromiseById(@PathParam("id") Long serviceabilityId, @QueryParam("weight") Double weight) {
        if (weight == null) {
            throw new BadRequestException("Missing weight");
        }
        Serviceability serviceability = Serviceability.findById(serviceabilityId);
        if (serviceability == null) {
            throw new NotFoundException();
        }
        return new ServiceResult(serviceability, weight);
    }

}
