package com.slx.etl.persist;

import com.slx.etl.bean.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by nitinka on 13/10/14.
 */
public class ServiceQueue {
    private static LinkedBlockingQueue<Service> servicesToStore = new LinkedBlockingQueue<>();
    private static Logger logger = LoggerFactory.getLogger(ServiceQueue.class);
    public static void put(Service Service) throws InterruptedException {
        synchronized (servicesToStore) {
            servicesToStore.put(Service);
        }
    }

    public static Service take() throws InterruptedException {
        logger.info("Queue Size Before Removal: "+servicesToStore.size());
        synchronized (servicesToStore) {
            return servicesToStore.poll(20, TimeUnit.SECONDS);
        }
    }
}
