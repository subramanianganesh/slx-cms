package com.slx.etl.persist;

import com.slx.etl.bean.Service;
import com.slx.etl.util.ElasticSearchUtil;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.index.IndexResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by nitinka on 13/10/14.
 */
public class ServiceQueueConsumerThread extends Thread{
    private static Logger logger = LoggerFactory.getLogger(ServiceQueueConsumerThread.class);
    private List<ServicePersistence> servicePersistenceList;
    public ServiceQueueConsumerThread(List<ServicePersistence> servicePersistenceList) {
        this.servicePersistenceList = servicePersistenceList;
    }

    public void run(){
        logger.info("Consumer Thread Started");
        int count = 1;
        while(true) {
            try {
                Service service = ServiceQueue.take();
                if(service == null) {
                    logger.info("Empty Record Received. Stopping");
                    break;
                }

                for(ServicePersistence sp : servicePersistenceList) {
                    sp.persist(service);
                }
                logger.info("Stored: "+count++);

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (JsonGenerationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
