package com.slx.etl.persist;

import com.slx.etl.bean.Service;
import com.slx.etl.util.ObjectMapperUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by nitinka on 19/10/14.
 */
public class PrintPersistenceImpl implements ServicePersistence {
    private static Logger logger = LoggerFactory.getLogger(PrintPersistenceImpl.class);

    @Override
    public void persist(Service service) throws IOException {
        logger.info(ObjectMapperUtil.INSTANCE.writeValueAsString(service));
    }
}
