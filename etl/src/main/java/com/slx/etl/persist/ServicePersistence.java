package com.slx.etl.persist;

import com.slx.etl.bean.Service;
import org.codehaus.jackson.JsonGenerationException;

import java.io.IOException;

/**
 * Created by nitinka on 19/10/14.
 */
public interface ServicePersistence {
    public void persist(Service service) throws IOException;
}
