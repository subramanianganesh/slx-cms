package com.slx.etl;

import com.slx.etl.bean.DestinationPincodeData;
import com.slx.etl.bean.Service;
import com.slx.etl.bean.Slot;
import com.slx.etl.persist.PrintPersistenceImpl;
import com.slx.etl.persist.ServicePersistence;
import com.slx.etl.persist.ServiceQueue;
import com.slx.etl.persist.ServiceQueueConsumerThread;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Time;
import java.util.*;

/**
 * Created by nitinka on 13/10/14.
 */
public class CreateBlueDartServices {
    private static Logger logger = LoggerFactory.getLogger(CreateBlueDartServices.class);
    private static String SOURCE_FILE = "./etl/data/BlueDart/BangaloreToPanIndiaPincodes.xlsx";

    public static void main(String[] args) throws IOException, InterruptedException {
        File excel =  new File (SOURCE_FILE);
        FileInputStream fis = new FileInputStream(excel);
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        /**
         * Source Pincode for the city
         */
        List<Integer> sourcePincodes = parseSourcePincodes(wb);

        /**
         * Meta Logistic Service Detail. City to Different Zones for different Shipping mode and shipment type
         */
        List<Service> metaServices = parsePricing(wb);

        /**
         * Destination Zone to Meta LogisticService Map
         */
        Map<String, List<Service>> desZoneMetaLogisticServicesMap = buildDesZoneToLogisticsServicesMap(metaServices);

        /**
         * State to Zone map for Dart Apex Cargo
         */
        Map<String, String> dartApexStateToZoneMap = parseZones(wb, "DartAir Zone States");

        /**
         * State to Zone map for Dart Surface Cargo
         */
        Map<String, String> dartSurfaceStateToZoneMap = parseZones(wb, "DartSurface Zone States");

        Map<String, List<String>> ignoreStatesForService = parseServiceStateIgnoreList(wb);

        ServiceQueueConsumerThread consumerThread = new ServiceQueueConsumerThread(Arrays.asList(new ServicePersistence[]{new PrintPersistenceImpl()}));
        consumerThread.start();

        for(Integer sourcePincode : sourcePincodes) {
            XSSFSheet pincodeServiceabilitySheet = wb.getSheet("Pincode Serviceability");
            int destinationPincodes = pincodeServiceabilitySheet.getLastRowNum() + 1;
            for(int dp=1; dp<=destinationPincodes ;dp++) {
                XSSFRow dpRow = pincodeServiceabilitySheet.getRow(dp);
                if(dpRow != null) {
                    DestinationPincodeData dpData = buildDestinationPincodeData(dpRow);

                    /**
                     * Create DP Service
                     */

                    if(dpData.isDomesticPriorityAvailable()) {
                        String desZone = "ZONE "+dpData.getZone();
                        buildAndQueueServices(sourcePincode, dpData.getPincode(), desZoneMetaLogisticServicesMap.get(desZone));
                    }

                    /**
                     * Create Apex Service
                     */
                    if(dpData.isDartApexAvailable()) {
                        String state = dpData.getState();
                        if(ignoreStatesForService.get("DART APEX").contains(state))
                            continue;
                        String desZone = dartApexStateToZoneMap.get(state);
                        buildAndQueueServices(sourcePincode, dpData.getPincode(), desZoneMetaLogisticServicesMap.get(desZone));
                    }

                    /**
                     * Create Apex Service
                     */
                    if(dpData.isDartSurfaceAvailable()) {
                        String state = dpData.getState();
                        if(ignoreStatesForService.get("DART SURFACE").contains(state))
                            continue;
                        String desZone = dartSurfaceStateToZoneMap.get(state);
                        buildAndQueueServices(sourcePincode, dpData.getPincode(), desZoneMetaLogisticServicesMap.get(desZone));
                    }
                }
            }
        }
        consumerThread.join();
    }

    private static Map<String, List<String>> parseServiceStateIgnoreList(XSSFWorkbook wb) {
        Map<String, List<String>> serviceStateIgnoreListMap = new HashMap<String, List<String>>();

        XSSFSheet hs = wb.getSheet("Service State Ignore List");
        int rowNum = hs.getLastRowNum() + 1;

        for(int i = 0; i <=rowNum; i++){
            XSSFRow row = hs.getRow(i);
            if(row != null) {
                List<String> statesToIgnoreList = new ArrayList<String>();
                String serviceType = row.getCell(0).toString().toUpperCase();
                if(row.getCell(1) != null) {
                    String[] statesToIgnore = row.getCell(1).toString().split(",");
                    statesToIgnoreList = Arrays.asList(statesToIgnore);
                }
                serviceStateIgnoreListMap.put(serviceType, statesToIgnoreList);
            }

        }
        return serviceStateIgnoreListMap;
    }
    /**
     * Create Actual Service Line item
     * @param sourcePincode
     * @param destinationPincode
     * @param zoneMetaLogisticServices
     * @return
     */
    private static void buildAndQueueServices(Integer sourcePincode, Integer destinationPincode, List<Service> zoneMetaLogisticServices) throws InterruptedException {
        for(Service zoneMetaLogisticService : zoneMetaLogisticServices) {
            ServiceQueue.put(new Service.Builder()
                    .setSrcRegion(String.valueOf(sourcePincode))
                    .setSrcType("Pincode")
                    .setDesRegion(String.valueOf(destinationPincode))
                    .setDesType("Pincode")
                    .setPartnerName(zoneMetaLogisticService.getPartnerName())
                    .setServiceType(zoneMetaLogisticService.getServiceType())
                    .setShipmentType(zoneMetaLogisticService.getShipmentType())
                    .setMaximumWeight(zoneMetaLogisticService.getMaximumWeight())
                    .setMinimumWeight(zoneMetaLogisticService.getMinimumWeight())
                    .setMinimumPrice(zoneMetaLogisticService.getMinimumPrice())
                    .setAdditionalWeight(zoneMetaLogisticService.getAdditionalWeight())
                    .setAdditionalWeightPrice(zoneMetaLogisticService.getAdditionalWeightPrice())
                    .setPickupCutoffTime(zoneMetaLogisticService.getPickupCutoffTime())
                    .setPickupSlot(zoneMetaLogisticService.getPickupSlot())
                    .setMinimumPickUpSLADay(zoneMetaLogisticService.getMinimumPickUpSLADay())
                    .setMaximumPickUpSLADay(zoneMetaLogisticService.getMaximumPickUpSLADay())
                    .setMinimumDeliveryUpSLADay(zoneMetaLogisticService.getMinimumDeliveryUpSLADay())
                    .setMaximumDeliveryUpSLADay(zoneMetaLogisticService.getMaximumDeliveryUpSLADay())
                    .build());
        }
    }

    /**
     * Return State to Zone Map
     * @param wb
     * @return
     */
    private static Map<String, String> parseZones(XSSFWorkbook wb, String sheet) {
        Map<String, String> stateToZoneMap = new HashMap<String,String>();
        XSSFSheet hs = wb.getSheet(sheet);
        int rowNum = hs.getLastRowNum() + 1;

        for(int i = 1; i <=rowNum; i++){
            XSSFRow row = hs.getRow(i);
            if(row != null) {
                String zone = row.getCell(0).toString().trim().toUpperCase();
                String[] states = row.getCell(1).toString().split(",");

                for(String state : states) {
                    stateToZoneMap.put(state.trim().toUpperCase(), zone);
                    stateToZoneMap.put(state.trim().toUpperCase().replace(" ",""), zone);
                    if(state.contains("&")) {
                        stateToZoneMap.put(state.trim().toUpperCase().replace("&", "AND"), zone);
                        stateToZoneMap.put(state.trim().toUpperCase().replace("& ", ""), zone);
                    }

                    if(state.contains("AND")) {
                        stateToZoneMap.put(state.trim().toUpperCase().replace("AND", "&"), zone);
                        stateToZoneMap.put(state.trim().toUpperCase().replace("AND ", ""), zone);
                    }
                }
            }
        }
        return stateToZoneMap;
    }

    private static Map<String, List<Service>> buildDesZoneToLogisticsServicesMap(List<Service> metaPricingRecords) {
        Map<String, List<Service>> desZoneToLogisticsServiceMap = new HashMap<String, List<Service>>();

        for(Service metaPricingRecord : metaPricingRecords) {
            String zone = metaPricingRecord.getDesRegion().trim().toUpperCase();
            List<Service> zoneServices = desZoneToLogisticsServiceMap.get(zone);
            if(zoneServices == null) {
                zoneServices = new ArrayList<Service>();
            }
            zoneServices.add(metaPricingRecord);
            desZoneToLogisticsServiceMap.put(zone, zoneServices);
        }

        return desZoneToLogisticsServiceMap;
    }

    private static List<Service> parsePricing(XSSFWorkbook wb) {
        List<Service> metaPricingRecords = new ArrayList<Service>();
        XSSFSheet hs = wb.getSheet("Pricing");
        int rowNum = hs.getLastRowNum() + 1;

        for(int i = 1; i <=rowNum; i++){
            XSSFRow row = hs.getRow(i);
            if(row != null) {
                Service ls = new Service.Builder()
                        .setSrcRegion(row.getCell(0).toString())
                        .setSrcType(row.getCell(1).toString())
                        .setDesRegion(row.getCell(2).toString())
                        .setDesType(row.getCell(3).toString())
                        .setPartnerName(row.getCell(4).toString())
                        .setServiceType(row.getCell(5).toString())
                        .setShipmentType(row.getCell(6).toString())
                        .setMaximumWeight(Float.parseFloat(row.getCell(7).toString()))
                        .setMinimumWeight(Float.parseFloat(row.getCell(8).toString()))
                        .setMinimumPrice(Float.parseFloat(row.getCell(9).toString()))
                        .setAdditionalWeight(Float.parseFloat(row.getCell(10).toString()))
                        .setAdditionalWeightPrice(Float.parseFloat(row.getCell(11).toString()))
                        .setPickupCutoffTime(Time.valueOf(row.getCell(12).toString()))
                        .setPickupSlot(new Slot(Time.valueOf(row.getCell(13).toString().split("-")[0]), Time.valueOf(row.getCell(13).toString().split("-")[1])))
                        .setMinimumPickUpSLADay((int) Float.parseFloat(row.getCell(14).toString()))
                        .setMaximumPickUpSLADay((int) Float.parseFloat(row.getCell(15).toString()))
                        .setMinimumDeliveryUpSLADay((int) Float.parseFloat(row.getCell(16).toString()))
                        .setMaximumDeliveryUpSLADay((int) Float.parseFloat(row.getCell(17).toString()))
                        .build();
                metaPricingRecords.add(ls);
            }

        }
        return metaPricingRecords;
    }

    private static DestinationPincodeData buildDestinationPincodeData(XSSFRow dpRow) {
        return new DestinationPincodeData((int)(Float.parseFloat(dpRow.getCell(2).toString()))
                , dpRow.getCell(1).toString().trim().toUpperCase()
                , dpRow.getCell(8).toString().trim().toUpperCase()
                , yesNoToBoolean(dpRow.getCell(9).toString())
                , yesNoToBoolean(dpRow.getCell(10).toString())
                , yesNoToBoolean(dpRow.getCell(11).toString()));
    }

    private static List<Integer> parseSourcePincodes(XSSFWorkbook wb) {
        List<Integer> sourcePincodes = new ArrayList<Integer>();
        XSSFSheet hs = wb.getSheet("Source Pincodes");
        int rowNum = hs.getLastRowNum() + 1;

        for(int i = 1; i <=rowNum; i++){
            XSSFRow row = hs.getRow(i);
            if(row != null)
                sourcePincodes.add((int)(Float.parseFloat(row.getCell(0).toString())));
        }
        return sourcePincodes;
    }
    private static boolean yesNoToBoolean(String s) {
        return s.toUpperCase().equals("YES") ? true : false;
    }

}
