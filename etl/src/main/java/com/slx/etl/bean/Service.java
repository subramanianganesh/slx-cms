package com.slx.etl.bean;

import java.sql.Time;

/**
 * Created by nitinka on 18/10/14.
 */
public class Service {
    private String srcRegion;
    private String srcType;
    private String desRegion;
    private String desType;
    private String partnerName;
    private String serviceType;
    private String shipmentType;
    private float maximumWeight;
    private float minimumWeight;
    private float minimumPrice;
    private float additionalWeight;
    private float additionalWeightPrice;
    private Time pickupCutoffTime;
    private Slot pickupSlot;
    private int minimumPickUpSLADay; // 0 if its same day, 1 for next day
    private int maximumPickUpSLADay; // 0 if its same day, 1 for next day
    private int minimumDeliveryUpSLADay; // 0 if its same day, 1 for next day
    private int maximumDeliveryUpSLADay; // 0 if its same day, 1 for next day

    private Service(Builder builder) {
        this.srcRegion = builder.srcRegion;
        this.srcType = builder.srcType;
        this.desRegion = builder.desRegion;
        this.desType = builder.desType;
        this.partnerName = builder.partnerName;
        this.serviceType = builder.serviceType;
        this.shipmentType = builder.shipmentType;
        this.maximumWeight = builder.maximumWeight;
        this.minimumWeight = builder.minimumWeight;
        this.minimumPrice = builder.minimumPrice;
        this.additionalWeight = builder.additionalWeight;
        this.additionalWeightPrice = builder.additionalWeightPrice;
        this.pickupCutoffTime = builder.pickupCutoffTime;
        this.pickupSlot = builder.pickupSlot;
        this.minimumPickUpSLADay =  builder.minimumPickUpSLADay;
        this.maximumPickUpSLADay =  builder.maximumPickUpSLADay;
        this.minimumDeliveryUpSLADay =  builder.minimumDeliveryUpSLADay;
        this.maximumDeliveryUpSLADay =  builder.maximumDeliveryUpSLADay;
    }

    public String getSrcType() {
        return srcType;
    }

    public String getDesType() {
        return desType;
    }

    public String getSrcRegion() {
        return srcRegion;
    }

    public String getDesRegion() {
        return desRegion;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public float getMaximumWeight() {
        return maximumWeight;
    }

    public float getMinimumWeight() {
        return minimumWeight;
    }

    public float getMinimumPrice() {
        return minimumPrice;
    }

    public float getAdditionalWeight() {
        return additionalWeight;
    }

    public float getAdditionalWeightPrice() {
        return additionalWeightPrice;
    }

    public Time getPickupCutoffTime() {
        return pickupCutoffTime;
    }

    public Slot getPickupSlot() {
        return pickupSlot;
    }

    public int getMinimumPickUpSLADay() {
        return minimumPickUpSLADay;
    }

    public int getMaximumPickUpSLADay() {
        return maximumPickUpSLADay;
    }

    public int getMinimumDeliveryUpSLADay() {
        return minimumDeliveryUpSLADay;
    }

    public int getMaximumDeliveryUpSLADay() {
        return maximumDeliveryUpSLADay;
    }

    public static class Builder {
        private String srcRegion;
        private String srcType;
        private String desRegion;
        private String desType;
        private String partnerName;
        private String serviceType;
        private String shipmentType;
        private float maximumWeight;
        private float minimumWeight;
        private float minimumPrice;
        private float additionalWeight;
        private float additionalWeightPrice;
        private Time pickupCutoffTime;
        private Slot pickupSlot;
        private int minimumPickUpSLADay; // 0 if its same day, 1 for next day
        private int maximumPickUpSLADay; // 0 if its same day, 1 for next day
        private int minimumDeliveryUpSLADay; // 0 if its same day, 1 for next day
        private int maximumDeliveryUpSLADay; // 0 if its same day, 1 for next day

        public Builder setSrcType(String srcType) {
            this.srcType = srcType;
            return this;
        }

        public Builder setDesType(String desType) {
            this.desType = desType;
            return this;
        }

        public Builder setSrcRegion(String srcRegion) {
            this.srcRegion = srcRegion;
            return this;
        }

        public Builder setDesRegion(String desRegion) {
            this.desRegion = desRegion;
            return this;
        }

        public Builder setPartnerName(String partnerName) {
            this.partnerName = partnerName;
            return this;
        }

        public Builder setServiceType(String serviceType) {
            this.serviceType = serviceType;
            return this;
        }

        public Builder setShipmentType(String shipmentType) {
            this.shipmentType = shipmentType;
            return this;
        }

        public Builder setMaximumWeight(float maximumWeight) {
            this.maximumWeight = maximumWeight;
            return this;
        }

        public Builder setMinimumWeight(float minimumWeight) {
            this.minimumWeight = minimumWeight;
            return this;
        }

        public Builder setMinimumPrice(float minimumPrice) {
            this.minimumPrice = minimumPrice;
            return this;
        }

        public Builder setAdditionalWeight(float additionalWeight) {
            this.additionalWeight = additionalWeight;
            return this;
        }

        public Builder setAdditionalWeightPrice(float additionalWeightPrice) {
            this.additionalWeightPrice = additionalWeightPrice;
            return this;
        }

        public Builder setPickupCutoffTime(Time pickupCutoffTime) {
            this.pickupCutoffTime = pickupCutoffTime;
            return this;
        }

        public Builder setPickupSlot(Slot pickupSlot) {
            this.pickupSlot = pickupSlot;
            return this;
        }

        public Builder setMinimumPickUpSLADay(int minimumPickUpSLADay) {
            this.minimumPickUpSLADay = minimumPickUpSLADay;
            return this;
        }

        public Builder setMaximumPickUpSLADay(int maximumPickUpSLADay) {
            this.maximumPickUpSLADay = maximumPickUpSLADay;
            return this;
        }

        public Builder setMinimumDeliveryUpSLADay(int minimumDeliveryUpSLADay) {
            this.minimumDeliveryUpSLADay = minimumDeliveryUpSLADay;
            return this;
        }

        public Builder setMaximumDeliveryUpSLADay(int maximumDeliveryUpSLADay) {
            this.maximumDeliveryUpSLADay = maximumDeliveryUpSLADay;
            return this;
        }

        public Service build() {
            return new Service(this);
        }
    }
}
