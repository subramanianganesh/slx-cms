package com.slx.etl.bean;

/**
 * Created by nitinka on 18/10/14.
 */
public class DestinationPincodeData {
    private int pincode;
    private String state;
    private String zone; // Zone A, Zone B , Zone C : These values are used only in case of DP
    private boolean domesticPriorityAvailable;
    private boolean dartApexAvailable;
    private boolean dartSurfaceAvailable;

    public DestinationPincodeData(int pincode, String state, String zone, boolean domesticPriorityAvailable, boolean dartApexAvailable, boolean dartSurfaceAvailable) {
        this.pincode = pincode;
        this.state = state;
        this.zone = zone;
        this.domesticPriorityAvailable = domesticPriorityAvailable;
        this.dartApexAvailable = dartApexAvailable;
        this.dartSurfaceAvailable = dartSurfaceAvailable;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public boolean isDomesticPriorityAvailable() {
        return domesticPriorityAvailable;
    }

    public void setDomesticPriorityAvailable(boolean domesticPriorityAvailable) {
        this.domesticPriorityAvailable = domesticPriorityAvailable;
    }

    public boolean isDartApexAvailable() {
        return dartApexAvailable;
    }

    public void setDartApexAvailable(boolean dartApexAvailable) {
        this.dartApexAvailable = dartApexAvailable;
    }

    public boolean isDartSurfaceAvailable() {
        return dartSurfaceAvailable;
    }

    public void setDartSurfaceAvailable(boolean dartSurfaceAvailable) {
        this.dartSurfaceAvailable = dartSurfaceAvailable;
    }
}
