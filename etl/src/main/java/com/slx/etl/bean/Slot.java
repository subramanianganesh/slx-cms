package com.slx.etl.bean;

import java.sql.Time;

/**
 * Created by nitinka on 18/10/14.
 */
public class Slot {
    private Time start;
    private Time end;

    public Slot(Time start, Time end) {
        this.start = start;
        this.end = end;
    }

    public Time getStart() {
        return start;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public Time getEnd() {
        return end;
    }

    public void setEnd(Time end) {
        this.end = end;
    }
}
