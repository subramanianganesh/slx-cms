package com.slx.etl.util;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;

import static org.elasticsearch.node.NodeBuilder.*;

/**
 * Created by nitinka on 13/10/14.
 */
public class ElasticSearchUtil {
    public static Client CLIENT = new TransportClient()
            .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));

    public static void main(String[] args) {
        SearchResponse response = CLIENT.prepareSearch("geo")
                .setTypes("pincode")
                .setQuery(QueryBuilders.termQuery("city", "BANGALORE"))
                .setFrom(0).setSize(1000)
                .setExplain(true)
                .execute()
                .actionGet();
        System.out.println("Hello");
    }
}
